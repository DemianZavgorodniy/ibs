package gluecode;


import io.cucumber.java.en.And;
import io.cucumber.java.en.Then;
import io.cucumber.java.en.When;
import io.restassured.RestAssured;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.Assert;

import java.util.ArrayList;
import java.util.List;


public class TestSteps {
    private static final String BASE_URL = "http://bank.ru";
    private static Response response;
    private static String responseJsonString;


    @When("Send a POST HTTP request with cardId = {int}")
    public void sendAPOSTHTTPRequestWithCardId(int cardId) {
        RestAssured.baseURI = BASE_URL;
        RequestSpecification request = RestAssured.given();
        request.header("Content-Type", "application/json");
        response = request.body("{ \"extendedInfo\": true, \"cards\": [ { \"cardId\": " + cardId + " } ] }")
                .post("/list/extended");

        responseJsonString = response.asString();
    }

    @Then("Receive valid card information with tag cost = {string}")
    public void receiveValidCardInformationWithTagCost(String cost) {
        Assert.assertTrue(responseJsonString.contains("\"cost\": \"" + cost + "\""));
    }

    @And("Tag operName = {string}")
    public void tagOperName(String operName) {
        Assert.assertTrue(responseJsonString.contains("\"operName\": \"" + operName + "\""));
    }
    @Then("Receive invalid card information with tag error where uuid = {string}, code = {string}, system = {string}, title = {string}, text = {string}")
    public void receiveInvalidCardInformationWithTagError(String uuid, String code, String system, String title, String text) {
        Assert.assertTrue(responseJsonString.contains("\"error\": " +
                                                                    "{ \"uuid\": \"" + uuid + "\", " +
                                                                    "\"code\": \"" + code + "\", " +
                                                                    "\"system\": \"" + system + "\", " +
                                                                    "\"title\": \"" + title + "\", " +
                                                                    "\"text\": \"" + text + "\"" + "}"));
    }
}
