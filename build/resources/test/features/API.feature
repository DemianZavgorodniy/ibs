Feature: Api testing for IBS

  Scenario: Sending valid cardId in range 0-9999
    When Send a POST HTTP request with cardId = 0
    Then Receive valid card information with tag cost = "0"
    And  Tag operName = "mts"
    When Send a POST HTTP request with cardId = 9999
    Then Receive valid card information with tag cost = "60"
    And  Tag operName = "beeline"

  Scenario: Sending invalid cardId out of range 0-9999
    When Send a POST HTTP request with cardId = 10000
    Then Receive invalid card information with tag error where uuid = "aaa", code = "bbb", system = "ccc", title = "ddd", text = "eee"

#      |uuid|aaa|
#      |code|bbb|
#      |system|ccc|
#      |title|ddd|
#      |text|eee|


